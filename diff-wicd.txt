--- /usr/lib/systemd/system/wicd.service	2012-12-21 00:53:53.000000000 -0200
+++ /etc/systemd/system/wicd.service	2013-05-05 23:06:03.804832840 -0300
@@ -1,14 +1,10 @@
 [Unit]
-Description=Wicd a wireless and wired network manager for Linux
-After=syslog.target
-Wants=network.target
-Before=network.target
+Description=Wicd Network Manager
 
 [Service]
 Type=dbus
 BusName=org.wicd.daemon
-ExecStart=/usr/sbin/wicd --no-daemon
+ExecStart=/usr/sbin/wicd -f
 
 [Install]
-WantedBy=multi-user.target
-Alias=dbus-org.wicd.daemon.service
+WantedBy=network.target
