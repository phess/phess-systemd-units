#!/bin/bash

##################################################################
# Este script verifica se algum script aqui neste diretório é
# redundante com os "oficiais" em /usr/lib/systemd/system/.
##################################################################

DISTDIR=/usr/lib/systemd/system
ADMINDIR=/etc/systemd/system

cd $ADMINDIR
for serv in *.service; do
    distserv=${DISTDIR}/$serv
    justname=${serv%%.*}
    if [ -f ${distserv} ]; then
        echo -n ${serv} duplicado
	if [[ `diff -q ${distserv} ${serv} &>/dev/null` ]]; then
	    echo " e SÃO IGUAIS"
	    listaapagar="$listaapagar $serv"
	else
	    echo " mas são diferentes"
	    diff -ruN ${DISTDIR}/${serv} ${ADMINDIR}/${serv} &> ${ADMINDIR}/diff-${justname}.txt
	fi
    fi
done

echo "Unit files que podem ser apagados: ${listaapagar}"

exit 0

# vim:et ai ts=4 sw=4
